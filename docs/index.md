---
title: "Accueil"
---

# {{ lecture_name }} {{ year }}

<img src="assets/images/logo_heiafr_color.png">

Bienvenue sur le site du cours "{{ lecture_name }} {{ year }}" ...

!!! info "URL pour ce site"
    <span style="font-size: 1.1rem;">{{ config.site_url }}</span>
