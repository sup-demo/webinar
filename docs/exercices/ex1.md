---
title: Exercice 1
week_no: 1
---

# Exercice 1 : ...

Nunc dui libero, rutrum non fermentum quis, imperdiet sit amet libero.

!!! tip "Astuce"
    Maecenas est quam, convallis a mi a, egestas ultrices enim. Nulla
    facilisis sem non mattis blandit. Aenean in diam vitae lectus
    tincidunt dignissim. Nulla porttitor rutrum nibh, id sodales erat
    imperdiet eget. Orci varius natoque penatibus et magnis dis
    parturient montes, nascetur ridiculus mus.
