import logging
import os
from pathlib import Path
from subprocess import call

import jinja2

logger = logging.getLogger("mkdocs.hooks")


def on_pre_build(config):
    logger.info("Running on_pre_build hook")
    pdf_to_jpg = []
    if "extra" in config:
        if "pdf_to_jpg" in config["extra"]:
            pdf_to_jpg = config["extra"]["pdf_to_jpg"]
    if not isinstance(pdf_to_jpg, list):
        pdf_to_jpg = [pdf_to_jpg]

    for root, _, files in os.walk(config["docs_dir"], topdown=False):
        env = jinja2.Environment(
            loader=jinja2.FileSystemLoader(root),
        )
        for name in files:
            if name == "pages.j2":
                template = env.get_template(name)
                content = template.render(config=config, env=os.environ)
                dst = Path(root) / ".pages"
                # Do not write the file if the content is the same.
                # Otherwise, the "watcher" will continuously reload the page.
                if dst.exists():
                    with open(dst) as f:
                        orig = f.read()
                    if content == orig:
                        continue
                with open(dst, "w") as f:
                    logger.info(f"Writing {dst}")
                    f.write(content)

            path = Path(root) / name
            if any(path.match(i) for i in pdf_to_jpg):
                dst = path.with_suffix(".jpg")
                if not dst.exists() or path.stat().st_mtime > dst.stat().st_mtime:
                    call("convert " + str(path) + "[0] " + str(dst), shell=True)
